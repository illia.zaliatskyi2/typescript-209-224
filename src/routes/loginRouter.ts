import { Request, Response, Router, NextFunction } from 'express'

interface requestWithBody extends Request {
    body: {
        [key: string]: string | undefined
    }
}

const requireAuth = (req: Request, res: Response, next: NextFunction) => {
    if (req.session && req.session.loggedIn) next()

    res.status(403)
    res.send('not permitted')
}

const router = Router()

router.get('/login', (req: requestWithBody, res: Response) => {
    res.send(`
        <form method="post">
            <div>
                <label for="email">Email</label>
                <input id="email" name="email" />
            </div>
            <div>
                <label for="password">Password</label>
                <input name="password" id="password" />
            </div>
            <button>Submit</button>
        </form>
    `)
})

router.post('/login', (req: Request, res: Response) => {
    const { email, password } = req.body

    if (email && password) {
        req.session = { loggedIn: true }
        res.redirect('/')
    } else {
        res.send('some fields are empty')
    }
})

router.get('/', (req: Request, res: Response) => {
    if (req.session && req.session.loggedIn) {
        res.send(`
            <div>
                <div>you are logged in</div>
                <a href="/logout">logout</a>
            </div>
        `)
    } else {
        res.send(`
            <div>
                <div>you are not logged in</div>
                <a href="/login">login</a>
            </div>
        `)
    }
})

router.get('/logout', (req: Request, res: Response) => {
    req.session = undefined
    res.redirect('/')
})

router.get('/protected', requireAuth, (req: Request, res: Response) => {
    res.send('welcome to protected route')
})

export { router }

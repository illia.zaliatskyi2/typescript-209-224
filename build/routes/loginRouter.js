"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const express_1 = require("express");
const requireAuth = (req, res, next) => {
    if (req.session && req.session.loggedIn)
        next();
    res.status(403);
    res.send('not permitted');
};
const router = (0, express_1.Router)();
exports.router = router;
router.get('/login', (req, res) => {
    res.send(`
        <form method="post">
            <div>
                <label for="email">Email</label>
                <input id="email" name="email" />
            </div>
            <div>
                <label for="password">Password</label>
                <input name="password" id="password" />
            </div>
            <button>Submit</button>
        </form>
    `);
});
router.post('/login', (req, res) => {
    const { email, password } = req.body;
    if (email && password) {
        req.session = { loggedIn: true };
        res.redirect('/');
    }
    else {
        res.send('some fields are empty');
    }
});
router.get('/', (req, res) => {
    if (req.session && req.session.loggedIn) {
        res.send(`
            <div>
                <div>you are logged in</div>
                <a href="/logout">logout</a>
            </div>
        `);
    }
    else {
        res.send(`
            <div>
                <div>you are not logged in</div>
                <a href="/login">login</a>
            </div>
        `);
    }
});
router.get('/logout', (req, res) => {
    req.session = undefined;
    res.redirect('/');
});
router.get('/protected', requireAuth, (req, res) => {
    res.send('welcome to protected route');
});
